const btn = document.querySelector('.btn')
const input = document.getElementById('input')
const inputForm = document.querySelector('.div-input')


btn.addEventListener('click',(e) => {
    e.preventDefault()
    console.log('eee');
    const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;

    const validadorEmail = emailRegex.test(input.value.trim())
    const inputVazio = input.value.trim()

    if(validadorEmail && inputVazio){
        input.classList.remove('error')
        removeMensagemErro()
    }else{
        input.classList.add('error')
        addMensagemErro('Please provide a valid email address')
    }
})


function addMensagemErro(msg){
    if(!document.querySelector('.erroText')){
        const spanErro = document.createElement('span')
        spanErro.classList.add('erroText')
        spanErro.innerText = msg
        inputForm.insertAdjacentElement('beforeend', spanErro)
    }

}

function removeMensagemErro(){
    const hasErro = document.querySelector('.erroText')
    if(hasErro){
        hasErro.remove()
    }
}
